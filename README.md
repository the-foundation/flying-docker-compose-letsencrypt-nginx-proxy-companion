Flying-docker-compose-letsencrypt-nginx-proxy-companion
==================================================

tuned docker-compose-letsencrypt-nginx-proxy-companion

## aiming for
* SSLLabs A-Grade and optimized stuff like:
* * native ipv6 support
* * http/2
* * etc.


## **SETUP:**

### **1st setup folder**

  ```
cd /path/to/compose/collection;
git clone --recurse-submodules  https://gitlab.com/the-foundation/flying-docker-compose-letsencrypt-nginx-proxy-companion.git  nginx-letsencrypt
cd nginx-letsencrypt
ln -s nginx.tmpl.YOURCHOICE nginx.tmpl

```
### **2nd configure .env **

#### !! change resolvers to e.g. 1.1.1.1 or 9.9.9.9 -> necessary for OCSP/CRL

  `nano nginx-letsencrypt/.env`

  ```
NGINX_FILES_PATH=/opt/nginx
RESOLVERS=9.9.9.9
SSL_POLICY=Mozilla-Modern
HSTS=max-age=1800
LETSENCRYPT_DEFAULT_EMAIL=default-mail-not-set@using-fallback-default.slmail.me
```

### **3rd setup nginx dhparam**

#### !! java 6/7 need (insecure) 1024 bit dhparam size !! LEGACY WARNING !! 
```
openssl dhparam -out /etc/nginx_dhparam.pem -dsaparam 4096
```

### **4th generate proxy network**

`docker network create nginx-proxy`
---


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/flying-docker-compose-letsencrypt-nginx-proxy-companion/README.md/logo.jpg" width="480" height="270"/></div></a>
